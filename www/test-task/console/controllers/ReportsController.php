<?php


namespace console\controllers;

use yii\console\Controller;
use yii\db\Query;

class ReportsController extends Controller
{
    public function actionReport() {
        $filename = 'report.csv';
        $offset = 0;
        $limit = 100;

        $data = [];
        if(file_exists($filename)) {
            $fp = fopen($filename, 'r');
            while (($row = fgetcsv($fp)) !== false) {
                $data[$row[0]] = $row;
            }
            fclose($fp);
        }

        $fp = fopen($filename, 'w');
        do {
            $rows = (new Query())
                ->select('c.email, c.name, SUM(o.cost) sumOrder')
                ->from('client c')
                ->leftJoin('order o', 'o.client_id=c.id')
                ->groupBy('c.id')
                ->limit($limit)
                ->offset($offset)
                ->all();

            $offset += $limit;

            foreach ($rows as $row) {
                if (isset($data[$row['email']])) {
                    $line = [$row['email'], $row['name'], $row['sumOrder']];
                    if($line != $data[$row['email']]) {
                        fputcsv($fp, $line);
                    }
                }
                else {
                    fputcsv($fp, $row);
                }
            }
        }
        while($rows);

        fclose($fp);
    }
}