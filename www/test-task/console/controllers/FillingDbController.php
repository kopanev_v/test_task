<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\db\Query;

class FillingDbController extends Controller
{
    public function actionFilling() {
        $names  = ['Иван', 'Степан', 'Сергей', 'Александр', 'Василий', 'Николай', 'Инокентий'];
        $emails = ['ya.ru', 'gmail.com', 'mail.ru'];

        for($i=0; $i < 1000; $i++) {
            $username = md5(rand(0, 99999999999));
            $email = $username.'@'.$emails[rand(0, count($emails)-1)];
            $name = $names[rand(0, count($names)-1)];

            $start = mktime(0,0,0,1,1,1980);
            $birthday = date('Y-m-d', rand($start, time()));

            Yii::$app->db->createCommand()->insert('client', [
                'username' => $username,
                'email'    => $email,
                'name'     => $name,
                'birthday' => $birthday,
            ])->execute();
        }


        for($i=0; $i < 1000; $i++) {
            $title = 'product name '.$i;
            $price = rand(500, 9999999);
            Yii::$app->db->createCommand()->insert('product', ['title'=> $title, 'price' => $price])->execute();
        }


        for($i=0; $i < 1000; $i++) {
            $clientId = rand(1, 1000);

            $start = mktime(0,0,0,1,1,2015);
            $createAt = date('Y-m-d H:i:s', rand($start, time()));

            Yii::$app->db->createCommand()->insert('order', [
                'client_id' => $clientId,
                'createAt'  => $createAt,
            ])->execute();
        }

        $orders = (new Query())->select('id')->from('order')->all();
        foreach ($orders as $order) {
            $countProduct = rand(1, 3);
            $costOrder = 0;
            for($i=0; $i < $countProduct; $i++) {
                $productId = rand(1, 1000);
                $product = (new Query())->select('id, price')->from('product')->where(['id' => $productId])->one();
                $count = rand(1, 9);
                Yii::$app->db->createCommand()->insert('order_item', [
                    'order_id'   => $order['id'],
                    'product_id' => $product['id'],
                    'price'      => $product['price'],
                    'count'      => $count,
                ])->execute();

                $costOrder += $product['price'] * $count;
            }

            Yii::$app->db->createCommand()->update('order', ['cost' => $costOrder], ['id' => $order['id']])->execute();
        }
    }
}