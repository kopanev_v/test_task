<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m200310_220358_create_table_client
 */
class m200310_220358_create_table_client extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('client', [
            'id'        => $this->primaryKey()->unsigned(),
            'username'  => $this->string(50)->notNull()->unique(),
            'email'     => $this->string(50)->notNull()->unique(),
            'name'      => $this->string(50)->notNull(),
            'birthday'  => $this->date()->null(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('client');

        return false;
    }

}
