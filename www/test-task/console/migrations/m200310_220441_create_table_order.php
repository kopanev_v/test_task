<?php

use yii\db\Migration;

/**
 * Class m200310_220441_create_table_order
 */
class m200310_220441_create_table_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('order', [
            'id'        => $this->primaryKey()->unsigned(),
            'client_id' => $this->integer()->unsigned()->notNull(),
            'cost'      => $this->float()->unsigned()->defaultValue(0),
            'createAt'  => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createTable('order_item', [
            'id'          => $this->primaryKey()->unsigned(),
            'order_id'    => $this->integer()->unsigned()->notNull(),
            'product_id'  => $this->integer()->unsigned()->notNull(),
            'price'       => $this->float()->unsigned()->notNull(),
            'count'       => $this->integer()->unsigned()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-order_item-order_id',
            'order_item',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_item');
        $this->dropTable('order');

        return false;
    }
}
