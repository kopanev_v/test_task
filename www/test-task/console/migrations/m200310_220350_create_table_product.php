<?php

use yii\db\Migration;

/**
 * Class m200310_220350_create_table_product
 */
class m200310_220350_create_table_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('product', [
            'id'    => $this->primaryKey()->unsigned(),
            'title' => $this->string(50)->notNull(),
            'price' => $this->float()->unsigned()->defaultValue(0),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');

        return false;
    }
}
