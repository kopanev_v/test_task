<?php


namespace backend\controllers;

use yii\web\Controller;
use backend\models\Report;

use common\models\Client;

class ReportController extends Controller
{
    public function actionReportB() {
        $report = new Report();
        $report->report('b');

        return $this->render('report');
    }

    public function actionReportC() {
        $report = new Report();
        $report->report('c');

        return $this->render('report');
    }
}