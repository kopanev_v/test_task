<?php

use yii\grid\GridView;

$grid_columns = [
    [
        'label' => '#',
        'attribute' => 'id',
    ],
    [
        'label' => 'Название',
        'attribute' => 'title',
    ],
    [
        'label' => 'Цена',
        'attribute' => 'price',
    ],
];

echo GridView::widget([
    'dataProvider' => $provider,
    'id' => 'product',
    'showFooter' => false,
    'summaryOptions' => ['class' => 'pull-right'],
    'caption' => "Товары",
    'captionOptions' => ['class' => 'h4 text-left text-info'],
    'footerRowOptions' => ['style' => 'font-weight:bold;text-decoration: underline;'],
    'tableOptions' => [
        'style' => 'width:100%;',
        'class' => 'table table-striped table-bordered',
    ],
    'columns' => $grid_columns,
]);