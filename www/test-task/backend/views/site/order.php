<?php

use yii\grid\GridView;

$grid_columns = [
    [
        'label' => '#',
        'attribute' => 'id',
    ],
    [
        'label' => 'Стоимость',
        'attribute' => 'cost',
    ],
    [
        'label' => 'Дата создания',
        'attribute' => 'createAt',
        'value' => function ($model) {
            return date("d-m-Y H:i:s", strtotime($model['createAt']));
        },
    ],
];

echo GridView::widget([
    'dataProvider' => $provider,
    'id' => 'orders',
    'showFooter' => false,
    'summaryOptions' => ['class' => 'pull-right'],
    'caption' => "Заказы",
    'captionOptions' => ['class' => 'h4 text-left text-info'],
    'footerRowOptions' => ['style' => 'font-weight:bold;text-decoration: underline;'],
    'tableOptions' => [
        'style' => 'width:100%;',
        'class' => 'table table-striped table-bordered',
    ],
    'columns' => $grid_columns,
]);