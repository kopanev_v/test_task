<?php

use yii\grid\GridView;

$grid_columns = [
    [
        'label' => '#',
        'attribute' => 'id',
    ],
    [
        'label' => 'username',
        'attribute' => 'username',
    ],
    [
        'label' => 'email',
        'attribute' => 'email',
    ],
    [
        'label' => 'Имя',
        'attribute' => 'name',
    ],
];

echo GridView::widget([
    'dataProvider' => $provider,
    'id' => 'client',
    'showFooter' => false,
    'summaryOptions' => ['class' => 'pull-right'],
    'caption' => "Клиенты",
    'captionOptions' => ['class' => 'h4 text-left text-info'],
    'footerRowOptions' => ['style' => 'font-weight:bold;text-decoration: underline;'],
    'tableOptions' => [
        'style' => 'width:100%;',
        'class' => 'table table-striped table-bordered',
    ],
    'columns' => $grid_columns,
]);