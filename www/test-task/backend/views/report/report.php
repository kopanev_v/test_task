<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


$form = ActiveForm::begin(['id' => 'report-b', 'method' => 'POST', 'options' => ['action' => Url::to()]]);

echo DatePicker::widget([
    "name" => "date_start",
    "options" => ["placeholder" => 'Дата оформления "от" (включительно)'],
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]);


echo DatePicker::widget([
    "name" => "date_end",
    "options" => ["placeholder" => 'Дата оформления "до" (включительно)'],
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]);

echo Html::submitButton('Выгрузить', ['class' => 'btn btn-primary']);

ActiveForm::end();
