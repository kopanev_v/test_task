<?php

namespace backend\models;

use Yii;
use common\helpers\FileForceDownload;
use yii\db\Query;

class Report
{
    public function report($type='b') {
        if(Yii::$app->request->post('date_start') && Yii::$app->request->post('date_end')) {
            $dStart = Yii::$app->request->post('date_start');
            $dEnd = Yii::$app->request->post('date_end');

            $phpExcel = new \PHPExcel();
            $phpExcel->setActiveSheetIndex(0);
            $asheet = $phpExcel->getActiveSheet();
            $asheet->getDefaultRowDimension();

            $begin = new \DateTime($dStart);
            $end = new \DateTime($dEnd);

            $numRowExcel = 1;

            while ($begin <= $end) {
                $mount = $begin->format('Y-m');
                $begin->modify('first day of next month');

                if($type == 'b') {
                    $this->typeB($mount, $asheet, $numRowExcel);
                }
                else {
                    $this->typeC($mount, $asheet, $numRowExcel);
                }
            }

            $file = 'report'.date('Y-m-d_H-i-s').'.xls';

            $obj_writer = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
            $obj_writer->save($file);
            FileForceDownload::init($file, true);
        }
    }

    private function typeB($mount, &$asheet, &$numRowExcel) {
        $asheet->setCellValueByColumnAndRow('0', $numRowExcel, $mount);
        $numRowExcel++;

        $orders = (new Query())
            ->select('c.email, AVG(o.cost) avgCost')
            ->from('order o')
            ->leftJoin('client c', 'o.client_id=c.id')
            ->where("o.createAt LIKE '{$mount}-%'")
            ->groupBy('o.client_id')
            ->all();

        foreach ($orders as $order) {
            $asheet->setCellValueByColumnAndRow('0', $numRowExcel, $order['email']);
            $asheet->setCellValueByColumnAndRow('1', $numRowExcel, $order['avgCost']);
            $numRowExcel++;
        }
    }

    private function typeC($mount, &$asheet, &$numRowExcel) {
        $ordersSum = (new Query())
            ->select('SUM(cost)')
            ->from('order')
            ->where("createAt LIKE '{$mount}-%'")
            ->scalar();

        $asheet->setCellValueByColumnAndRow('0', $numRowExcel, $mount);
        $asheet->setCellValueByColumnAndRow('1', $numRowExcel, $ordersSum);
        $numRowExcel++;
    }
}