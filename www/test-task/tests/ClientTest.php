<?php

namespace tests;

use common\models\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class ClientTest extends TestCase
{
    use TestCaseTrait;

    /**
     * @return \PHPUnit\DbUnit\Database\DefaultConnection
     */
    protected function getConnection() {
        $pdo = new \PDO('mysql:host=mysql;dbname=test_task_db', 'root', 'PASSWORD');
        return $this->createDefaultDBConnection($pdo, 'test_task_db');
    }

    /**
     * @return \PHPUnit\DbUnit\DataSet\FlatXmlDataSet
     */
    protected function getDataSet() {
        return $this->createFlatXMLDataSet(dirname(__FILE__).'/_data/client-init.xml');
    }


    public function testGetClientsBirthdaySameDay() {
        $client = Client::findOne(1);
        $client2 = $client->getClientsBirthdaySameDay()[0];
        $client3 = Client::findOne(2);

        $this->assertEquals($client->birthday, $client2->birthday);
        $this->assertNotEquals($client->birthday, $client3->birthday);
    }
}