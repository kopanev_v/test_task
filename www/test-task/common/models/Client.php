<?php


namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class Client
 * @package common\models
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $name
 * @property string $birthday
 */
class Client extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%client}}';
    }


    public function getClientsBirthdaySameDay() {
        return Client::find()->where(['birthday' => $this->birthday])->all();
    }
}