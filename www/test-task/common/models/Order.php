<?php


namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class Order
 * @package common\models
 *
 * @property int $id
 * @property int $client_id
 */
class Order extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @return array|ActiveRecord[]
     */
    public function getItems() {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id'])->all();
    }

    /**
     * @return array|ActiveRecord|null
     */
    public function getClient() {
        return $this->hasOne(Client::className(), ['id' => 'client_id'])->one();
    }

    /**
     * @return int
     */
    public function getTotalItems() {
        $totalItem = 0;
        foreach ($this->items as $item) {
            $totalItem += 1;
        }

        return $totalItem;
    }

    /**
     * @return float|int
     */
    public function getTotalPrice() {
        $totalPrice = 0;
        foreach ($this->items as $item) {
            $totalPrice += $item->price * $item->quantity;
        }

        return $totalPrice;
    }
}