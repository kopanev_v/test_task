<?php


namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class OrderItem
 * @package common\models
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $count
 * @property float $price
 */
class OrderItem extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_item}}';
    }

    /**
     * @return array|ActiveRecord|null
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->one();
    }

    /**
     * @return float|int
     */
    public function getCost() {
        return $this->count * $this->price;
    }
}