<?php


namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class Product
 * @package common\models
 *
 * @property int $id
 * @property string $title
 * @property float $price
 */
class Product extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product}}';
    }
}